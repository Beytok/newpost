<?php
class NewPost
{
	// nova powta api
	protected function requestNewPost($model, $method, $params = NULL, $language='ru',$apiKey) {

		$url = 'https://api.novaposhta.ua/v2.0/json/';

		$data = array(
			'apiKey' => $apiKey,
			'modelName' => $model,
			'calledMethod' => $method,
			'language' => $language,
			'methodProperties' => $params
		);

		$post = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$result = curl_exec($ch);
		curl_close($ch);

		return json_decode($result, 1);
	}

	private function GetNextTime(){
		return COption::GetOptionString("main",'NEW_POST_NEXT_TIME_UPDATE');
	}

	private function GetStep(){
		return COption::GetOptionString("main",'NEW_POST_NEXT_STEP');
	}

	private function GetSubStep(){
		return COption::GetOptionString("main",'NEW_POST_NEXT_SUB_STEP');
	}

	private function NextTime(){
		$date = date("d.m.Y")." 02:00:00";
		$stmp = MakeTimeStamp($date, "DD.MM.YYYY HH:MI::SS");
		$stmp = $stmp+604800;
		COption::SetOptionString("main",'NEW_POST_NEXT_TIME_UPDATE',$stmp);
		return true;
	}
	private function NextStep($step){
		if(is_int($step)){
			COption::SetOptionString("main",'NEW_POST_NEXT_STEP',$step);
			return true;
		}
		else{
			return false;
		}
	}
	private function NextSubStep($step){
		if(is_int($step)){
			COption::SetOptionString("main",'NEW_POST_NEXT_SUB_STEP',$step);
			return true;
		}
		else{
			return false;
		}
	}
	private function SendErrorNewPost($errorsAr){
		$error = "Обновление новой почти перенесено на 1 неделю причина:\n";
		foreach($errorsAr as $er){
			$error .=$er."\n";
		}
		mail("mail","NewPost",$error);
	}
	//update nova powta agent
	function UpdateNewPost(){

		$NextTime = self::GetNextTime();

		CModule::IncludeModule("iblock");
		CModule::IncludeModule("main");

		//если дата пуста или настало время обновления
		if(empty($NextTime) || $NextTime<time()){

			$arParams["IBLOCK_ID"] = 50;
			$arParams["STEP_DEACTIVE_SEC"] = 300;
			$arParams["STEP_SEC"] = 200;
			$arParams["STEP_DEACTIVE_EL"] = 300;
			$arParams["STEP_EL"] = 300;
			$arParams["FILE_CITIES"] = 'NewPostCitiesIntegrate.txt';
			$arParams["FILE_WAREHOUSE"] = 'NewPostWarehouseIntegrate.txt';
			$arParams["API_KEY"] = 'API_KEY';

			$step = self::GetStep();
			$subStep = self::GetSubStep();

			$step = ($step>0) ? $step : 1;
			$subStep = ($subStep>0) ? $subStep : 1;

			CModule::IncludeModule("iblock");

			switch($step){
				case "1":

					$resultCities = requestNewPost("Address", "getCities", array('Page'=>0,'FindByString'=>'','Ref'=>''), 'ru',$arParams["API_KEY"]);

					if($resultCities["success"]==true && empty($resultCities["errors"])){
						$fileCity = fopen($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_CITIES"],"w+");
						fwrite($fileCity,serialize($resultCities["data"]));
						fclose($fileCity);
						self::NextStep(2);
						self::NextSubStep(1);
					}
					else{
						self::SendErrorNewPost($resultCities["errors"]);
						self::NextTime();
					}
				break;
				case "2":

					$resultWarehouses = requestNewPost("Address", "getWarehouses", array('Page'=>0,'CityRef'=>''), 'ru',$arParams["API_KEY"]);

					if($resultWarehouses["success"]==true && empty($resultWarehouses["errors"])){
						$fileCity = fopen($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_WAREHOUSE"],"w+");
						fwrite($fileCity,serialize($resultWarehouses["data"]));
						fclose($fileCity);

						self::NextStep(3);
						self::NextSubStep(1);
					}
					else{
						self::SendErrorNewPost($resultWarehouses["errors"]);
						self::NextTime();
					}
				break;
				case "3":
					$countIBlockSection = CIBlockSection::GetCount(array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
					$subStep = self::GetSubStep();

					if(($arParams["STEP_DEACTIVE_SEC"]*$subStep-$arParams["STEP_DEACTIVE_SEC"])<$countIBlockSection){

						$resSection = CIBlockSection::GetList(
							array("ID"=>"DESC"),
							array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]),
							false,
							array("ID"),
							array("iNumPage"=>$subStep,"nPageSize"=>$arParams["STEP_DEACTIVE_SEC"])
						);

						$bs = new CIBlockSection;

						while($arSec = $resSection->Fetch()){
							$bs->Update($arSec["ID"],array("ACTIVE"=>"N"));
						}
						$subStep++;
						self::NextSubStep($subStep);
					}
					else{
						self::NextStep(4);
						self::NextSubStep(1);
					}

				break;
				case "4":

					$subStep = self::GetSubStep();

					$RangeStart = ($subStep==1) ? 0 : ($subStep-1)*$arParams["STEP_SEC"];

					$allCity = unserialize(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_CITIES"]));

					$scaning_array = array_slice($allCity,$RangeStart,$arParams["STEP_SEC"]);

					if(count($scaning_array)>0){
						foreach($scaning_array as $key => $arItem){

							$resReg = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"XML_ID"=>$arItem["Ref"]),false,array("ID","NAME","IBLOCK_ID"),array("nPageSize"=>"1","iNumPage"=>1))->Fetch();

							$bs = new CIBlockSection;

							if($resReg["ID"]>0){
								$arFields = Array(
									"ACTIVE" => "Y",
									"IBLOCK_ID" => $arParams["IBLOCK_ID"],
									"XML_ID" => $arItem["Ref"],
									"UF_NAME_RU" => trim($arItem["DescriptionRu"]),
									"NAME" => $arItem["Description"],
								);

								$res = $bs->Update($resReg["ID"], $arFields);
							}
							else{
								$arFields = Array(
									"NAME" => $arItem["Description"],
									"ACTIVE" => "Y",
									"IBLOCK_ID" => $arParams["IBLOCK_ID"],
									"XML_ID" => $arItem["Ref"],
									"UF_NAME_RU" => trim($arItem["DescriptionRu"]),
								);
								$res = $bs->add($arFields);
							}
						}
						$subStep++;
						self::NextSubStep($subStep);
					}
					else{
						self::NextStep(5);
						self::NextSubStep(1);
					}
				break;
				case "5":
					$countIBlockElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]),array());
					$subStep = self::GetSubStep();

					if(($arParams["STEP_DEACTIVE_EL"]*$subStep-$arParams["STEP_DEACTIVE_EL"])<$countIBlockElement){

						$resElements = CIBlockElement::GetList(
							array("ID"=>"DESC"),
							array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]),
							false,
							array("iNumPage"=>$subStep,"nPageSize"=>$arParams["STEP_DEACTIVE_EL"]),
							array("ID")
						);

						$el = new CIBlockElement;

						while($arEl = $resElements->Fetch()){
							$el->Update($arEl["ID"],array("ACTIVE"=>"N"));
						}
						$subStep++;
						self::NextSubStep($subStep);
					}
					else{
						self::NextStep(6);
						self::NextSubStep(1);
					}
				break;
				case "6":
					$subStep = self::GetSubStep();

					$RangeStart = ($subStep==1) ? 0 : ($subStep-1)*$arParams["STEP_EL"];

					$allWarehouse = unserialize(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_WAREHOUSE"]));

					$scaning_array = array_slice($allWarehouse,$RangeStart,$arParams["STEP_EL"]);

					if(count($scaning_array)>0){
						foreach($scaning_array as $key => $arItem){

							$name = trim($arItem["Description"]);

							$resEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"XML_ID"=>$arItem["Ref"]),false,array("nPageSize"=>"1","iNumPage"=>1),array("ID","NAME","IBLOCK_ID","IBLOCK_SECTION_ID"))->Fetch();

							$el = new CIBlockElement;
							$resSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"XML_ID"=>$arItem["CityRef"]),false,array("ID","NAME"),array("nPageSize"=>"1","iNumPage"=>1))->Fetch();


							$arFields = array();

							if($resEl["ID"]>0){

								if($resEl["IBLOCK_SECTION_ID"]!=$resSection["ID"]){$arFields["IBLOCK_SECTION_ID"] = $resSection["ID"];}
								$arFields["ACTIVE"] = "Y";
								$arFields["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
								$arFields["XML_ID"] = $arItem["Ref"];
								$arFields["NAME"] = $name;

								$res = $el->Update($resEl["ID"], $arFields);

								CIBlockElement::SetPropertyValuesEx($resEl["ID"], $arParams["IBLOCK_ID"], array("LONGIRUDE"=>$arItem["Longitude"] ,"LATITUDE"=>$arItem["Latitude"],"NAME_RU"=>$arItem["DescriptionRu"]));

							}
							else{
								$arFields["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
								$arFields["NAME"] = $name;
								$arFields["XML_ID"] = $arItem["Ref"];
								$arFields["IBLOCK_SECTION_ID"] = $resSection["ID"];
								$arFields["ACTIVE"] = "Y";


								$res = $el->add($arFields);

								CIBlockElement::SetPropertyValuesEx($res, $arParams["IBLOCK_ID"], array("LONGIRUDE"=>$arItem["Longitude"] ,"LATITUDE"=>$arItem["Latitude"],"NAME_RU"=>$arItem["DescriptionRu"]));

							}
						}
						$subStep++;
						self::NextSubStep($subStep);
					}
					else{
						self::NextStep(7);
						self::NextSubStep(1);
					}
				break;
				case "7":
					self::NextTime();
					self::NextStep(1);
					self::NextSubStep(1);
				break;
			}
		}
		return "NewPost::UpdateNewPost();";
	}
}
?>